const mongoose = require('mongoose');

var contactSchema = new mongoose.Schema({
  name: String,
  email: String,
  message: String,
  date: String
}, {collection: 'contacts'});

module.exports = mongoose.model('Contact', contactSchema);
