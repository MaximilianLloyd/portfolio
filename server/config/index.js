'use strict';

let port = '27017';
let db   = 'portfolio';

module.exports = {
  express: {
    port: 7777
  },
  mongo: {
    port: port,
    db: db,
    uri: 'mongodb://127.0.0.1:' + port + '/' + db
  }
};
