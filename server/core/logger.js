'use strict';

const winston = require('winston');

let config = {
  levels: {
    error: 0,
    warning: 1,
    debug: 2,
    data: 3,
    info: 4,
  },
  colors: {
    error: 'red',
    warning: 'yellow',
    debug: 'blue',
    data: 'grey',
    info: 'green',
  }
};

let logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      colorize: true
    }),
    new (winston.transports.File)({
      filename: '../logs/events.log',
      level: 'error'
    })
  ],
  levels: config.levels,
  colors: config.colors
});

module.exports = logger;
