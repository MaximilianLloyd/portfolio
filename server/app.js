'use strict';

const   express = require('express'),
        morgan  = require('morgan'),
        chalk   = require('chalk'),
        config  = require('./config'),
        mongoose = require('mongoose'),
        bodyParser = require('body-parser'),
        log     = require('./core/logger'),
        Contact = require('./models/email'),
        router  = express.Router(),
        app     = express();


// Setup
app.use(morgan('dev'));
app.use(express.static('../client/dist'));
app.use(bodyParser.json());

mongoose.connect(config.mongo.uri);


router.get('/', (req, res) => {
  res.sendFile('index.html', {root: './'});
});

router.get('/projects/:project', (req, res) => {
  let project = req.params.project;
  console.log('test');
  log.debug(`request for the view for project ${project.split(".")[0]}`);
  res.sendFile(`views/projects/${project}`, {'root': '../client'});
});

router.get('/api/cv', (req, res) => {
  log.debug('Request for cv recieved...');
  res.sendFile('cv.pdf', {'root': './assets'});
});

router.post('/api/contact', (req, res) => {
  log.debug(`Recieved email with following data: `);
  log.debug(` name: ${req.body.name}`);
  log.debug(` email: ${req.body.email}`);
  log.debug(` message: ${req.body.email}`);
  log.debug(` date: ${req.body.date}`);
  res.sendStatus(200);
  let contact = new Contact(req.body);
  contact.save((err, contact)  => {
  if (err) return console.error(err);
  log.debug(`Successfully saved contact ticket submited by ${req.body.name} <${req.body.email}>`);
});
});

router.get('/api/contacts', (req, res) => {
  Contact.find({},(err, contacts) => {
    let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    log.debug(`Sending contact data to client with ${chalk.green(ip)}`);
    res.send(contacts);
  });
});

router.get('*', (req, res) => {
  res.sendFile(`views/error/404.html`, {'root': '../client'});
});

// Logging some data
log.debug(`now listening on port ${chalk.green(config.express.port)}`);
log.debug(`connected to mongoDB at port ${chalk.green(config.mongo.port)}. Database name is ${config.mongo.db}`);

app.use(router);
app.listen(config.express.port);
