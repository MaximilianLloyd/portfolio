'use strict';

window.addEventListener('load', () => {
  let bars = document.querySelectorAll('.graph--line');
  let header = document.querySelector('#header');
  let about = document.querySelector('#about');

  function callback() {
    console.log('Listener');
    let scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    if(scrollTop > (header.offsetHeight + about.offsetHeight)) {
      for (var i = 0; i < bars.length; i++) {
        let percent = bars[i].getAttribute('data-percent');
        bars[i].children[1].style.transition = 'all 1200ms ease-in-out';
        bars[i].children[1].style.backgroundColor = getStyle(bars[i].children[0], 'color');
        bars[i].children[1].style.width += percent + '%';

      }
      window.removeEventListener('scroll', callback);
    }
  }
  window.addEventListener('scroll', callback);
});

function getStyle(el, styleProp) {
  var value, defaultView = (el.ownerDocument || document).defaultView;
  // W3C standard way:
  if (defaultView && defaultView.getComputedStyle) {
    // sanitize property name to css notation
    // (hypen separated words eg. font-Size)
    styleProp = styleProp.replace(/([A-Z])/g, "-$1").toLowerCase();
    return defaultView.getComputedStyle(el, null).getPropertyValue(styleProp);
  } else if (el.currentStyle) { // IE
    // sanitize property name to camelCase
    styleProp = styleProp.replace(/\-(\w)/g, function(str, letter) {
      return letter.toUpperCase();
    });
    value = el.currentStyle[styleProp];
    // convert other units to pixels on IE
    if (/^\d+(em|pt|%|ex)?$/i.test(value)) {
      return (function(value) {
        var oldLeft = el.style.left, oldRsLeft = el.runtimeStyle.left;
        el.runtimeStyle.left = el.currentStyle.left;
        el.style.left = value || 0;
        value = el.style.pixelLeft + "px";
        el.style.left = oldLeft;
        el.runtimeStyle.left = oldRsLeft;
        return value;
      })(value);
    }
    return value;
  }
}
