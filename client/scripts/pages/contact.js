'use strict';


let send;
let name;
let email;
let message;

window.addEventListener('load', () => {
  send = document.querySelector('.contact--cta');
  name = document.querySelector('.input--name');
  email = document.querySelector('.input--email');
  message = document.querySelector('.input--message');
});

function clearForm() {
  name.value    = "";
  email.value   = "";
  message.value = "";
}
