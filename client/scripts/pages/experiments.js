'use strict';

window.addEventListener('load', () => {
  let s_header = document.querySelector('#header');
  let s_about = document.querySelector('#about');
  let s_skills = document.querySelector('#header');

  let paths = document.querySelectorAll('path');

  function callback() {
    let scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    if(scrollTop > (s_header.offsetHeight + s_about.offsetHeight + s_skills.offsetHeight)) {
      for (var i = 0; i < paths.length; i++) {
        var path = paths[i];
        var length = path.getTotalLength();
        // Clear any previous transition
        path.style.transition = path.style.WebkitTransition =
          'none';
        // Set up the starting positions
        path.style.strokeDasharray = length + ' ' + length;
        path.style.strokeDashoffset = length;
        // Trigger a layout so styles are calculated & the browser
        // picks up the starting position before animating
        path.getBoundingClientRect();
        // Define our transition
        path.style.transition = path.style.WebkitTransition =
          'stroke-dashoffset 2s ease-in-out';
        // Go!
        path.style.strokeDashoffset = '0';
      }
      window.removeEventListener('scroll', callback);
    }
  }

  window.addEventListener('scroll', callback);

});
