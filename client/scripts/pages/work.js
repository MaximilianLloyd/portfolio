'use strict';

window.addEventListener('load', () => {
  let projects = document.querySelectorAll('.work--project');


  for (let i = 0; i < projects.length; i++) {
    projects[i].addEventListener('mouseover', (e) => {
      projects[i].children[0].style.transform = 'scale(1)';
    });
    projects[i].addEventListener('mouseleave', (e) => {
      projects[i].children[0].style.transform = 'scale(0)';
    });
  }
});
