'use strict';

window.addEventListener('load', () => {
  let close        = document.querySelector('.overlay--close');
  let overlay      = document.querySelector('.module--overlay');
  let container    = document.querySelector('.overlay--ajax--content');
  let projects_cta = document.querySelectorAll('.project--cta');
  let cache = [];
  let timeout = 800;

  overlay.style.transition = `all ${timeout}ms ease-in-out`;

  for (let i = 0; i < projects_cta.length; i++) {
    projects_cta[i].addEventListener('click', (e) => {
      let project = e.target.getAttribute('data-dir');

        document.body.style.overflow = 'hidden';
        document.body.style.padding = '0px 2px 0px 0px';

      let result = cache.filter((obj) => { return obj.name == project; });
      // if it is not in the cache
      if(!result.length) {
      axios.get(`http://maxlloyd.no/projects/${project}`)
        .then(function (res) {
          let html = res.data;
          container.innerHTML = html;
          cache.push({name: project, html:html});
          showOverlay();
          slider(e);
        })
        .catch(function (res) {
          showOverlay(overlay);
        });
      } else {
        console.log('Found page in cache');
        container.innerHTML = result[0].html;
        showOverlay(overlay);
        slider(e);
      }
    });
  }

  close.addEventListener('click', () => {
    console.log('Closing overlay');
    document.body.style.overflow = 'visible';
    document.body.style.padding = '0px 0px 0px 0px';
    overlay.style.webkitTransform = 'translateX(100vw)';
    overlay.style.mozTransform = 'translateX(100vw)';
    overlay.style.msTransform = 'translateX(100vw)';
    overlay.style.oTransform = 'translateX(100vw)';
    window.setTimeout(() => {
      container.innerHTML = '';
    }, timeout);
  });
});

function slider(e) {
  var is_slider = e.target.hasAttribute('data-slider');
  if(is_slider) {
  let slider         = document.querySelector('.module--slider');
  let slider_wrapper = document.querySelector('.slider--wrapper');
  let slides         = slider_wrapper.children;
  let timeout        = 3500;
  console.log(slides);

  // Setting it to 1 because 0 is already displaying
  var index = 1;
  setInterval(() => {
    // if the index is less than the amount of slides then:
    if(index < slider_wrapper.children.length) {
      slider_wrapper.children[index].scrollIntoView(true);
      index++;
    } else {
      index = 0;
    }
  }, timeout);

}
}

function showOverlay(overlay) {
  document.body.style.overflow = 'hidden';
  document.body.style.padding = '0px 0px 0px 0px';
  overlay.style.webkitTransform = 'translateX(0)';
  overlay.style.mozTransform = 'translateX(0)';
  overlay.style.msTransform = 'translateX(0)';
  overlay.style.oTransform = 'translateX(0)';
}
