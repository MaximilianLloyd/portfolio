'use strict';

window.addEventListener('load', () => {
  let navigation = document.querySelector('.navigation');
  let links      = document.querySelectorAll('.link a');
  let menu       = document.querySelector('.menu');
  let close = () => {
    navigation.style.transform = `translateX(${-navigation.offsetWidth}px)`;
    menu.children[0].style.transform = 'rotate(0deg)';
    menu.children[2].style.transform = 'rotate(0deg)';
    menu.children[0].style.margin = '10px 10px 10px 10px';
    menu.children[1].style.display   = 'block';
    menu_opened = false;
  };

  let breakpoint = 720;
  for (var i = 0; i < links.length; i++) {
    links[i].addEventListener('click', () => {
      close();
    });
    links[i].parentNode.addEventListener('click', (e) => {
      if(window.innerWidth < breakpoint) {
          let id = e.target.childNodes[0].href.split('/').pop();
          document.querySelector(id).scrollIntoView();
          close();
        }
    });
  }


});
