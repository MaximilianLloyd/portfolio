'use strict';

let menu_opened = false;
window.addEventListener('load', () => {
	let menu 		= document.querySelector('.menu');
	let navigation  = document.querySelector('.navigation');

	menu.addEventListener('click', (e) => {
		if(!menu_opened) {
			menu.children[0].style.transform = 'rotate(45deg)';
			menu.children[0].style.margin = '22.5px 10px -15px 10px';
			menu.children[1].style.display   = 'none';
			menu.children[2].style.transform = 'rotate(-45deg)';
			menu_opened = true;
			navigation.style.transform = `translateX(${0}px)`;
			navigation.style.boxShadow = '5px 0px 10px  rgba(0,0,0, 0.1)';
		} else {
			navigation.style.boxShadow = 'none';
			menu.children[0].style.transform = 'rotate(0deg)';
			menu.children[2].style.transform = 'rotate(0deg)';
			menu.children[0].style.margin = '10px 10px 10px 10px';
			menu.children[1].style.display   = 'block';
			menu_opened = false;
			change_color(menu.children, '#222831');
			navigation.style.transform = `translateX(${-navigation.offsetWidth}px)`;
		}
	});
});

function change_color(nodes, color) {
	for(let i = 0; i < nodes.length; i++) {
		nodes[i].style.backgroundColor = color;
	}
}
