const gulp = require('gulp'),
  sass = require('gulp-sass'),
  uglify = require('gulp-uglify'),
  babel = require('gulp-babel'),
  imageMin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  swig = require('gulp-swig'),
  htmlmin = require('gulp-minify-html'),
  plumber = require('gulp-plumber'),
  minifyCss = require('gulp-minify-css'),
  autoprefixer = require('gulp-autoprefixer'),
  browserSync = require('browser-sync');



gulp.task('js', () => {
  return gulp.src('scripts/**/*.js')
    .pipe(plumber())
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(uglify())
    .pipe(gulp.dest('dist/assets/scripts'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('sass', () => {
  return gulp.src("sass/index.sass")
    .pipe(plumber())
    .pipe(sass({
      style: 'expanded',
      includePaths: ['./sass/modules', './sass/pages']
    }))
    .pipe(autoprefixer())
    .pipe(minifyCss())
    .pipe(gulp.dest("./dist/assets/css"))
    .pipe(browserSync.stream());
});

gulp.task('browser-sync', () => {
  browserSync.init({
    proxy: "http://localhost:7777",
  });
});


gulp.task('views', function() {
  gulp.src('./views/**/*.html')
    .pipe(htmlmin())
    .pipe(gulp.dest('./dist/'))
    .pipe(swig())
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('index', function() {
  gulp.src('./index.html')
    .pipe(htmlmin())
    .pipe(gulp.dest('./dist/'))
    .pipe(swig())
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('image-min', () => {
  return gulp.src('media/**/*.*')
    .pipe(imageMin({
      progressive: true,
      svgoPlugins: [{
        removeViewBox: false
      }],
      use: [pngquant()]
    }))
    .pipe(gulp.dest('./dist/assets/media'));
});

gulp.task('watch', () => {
  gulp.watch("sass/**/*.sass", ["sass"]);
  gulp.watch('scripts/**/*.js', ['js']);
  gulp.watch('media/**/*.*', ['image-min']);
  gulp.watch('index.html', ['index']);
  gulp.watch('views/**/*.html', ['views']);
});

gulp.task('default', ['watch', 'browser-sync']);
